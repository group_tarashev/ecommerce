import { getProduct } from "../cookies/products";

export const productReducer = (product, action) => {
  switch (action.type) {
    case "added": {
      const check = product.find((item) => item.id == action.id);
      if (!check) {
        return [
          ...product,
          {
            id: action.id,
            title: action.title,
            desc: action.desc,
            price: action.price,
            discount: action.discount,
            act_price: action.act_price,
            image: action.image,
            count: action.count,
            size: action.size,
          },
        ];
      } else {
        if (check.size == action.size) {
          return product.map((item) => {
            return {
              ...item,
              count: item.id == action.id ? action.count : item.count,
            };
          });
        }
        else{
          return [
            ...product,
            {
              id: action.id ,
              title: action.title,
              desc: action.desc,
              price: action.price,
              discount: action.discount,
              act_price: action.act_price,
              image: action.image,
              count: action.count,
              size: action.size,
            },
          ];
        }
      }
    }
    case "remove": {
      const isExist = product.filter((item) => item.id !== action.id);
      return isExist;
    }
    case 'clear' : {
      return []
    }
    default:
      return product;
  }
};

export const initialProduct = getProduct() || [];
