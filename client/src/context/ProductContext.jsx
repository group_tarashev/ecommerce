import axios from "axios";
import { createContext, useEffect, useReducer, useState } from "react";
import { initialProduct, productReducer } from "../reducer/product";
import { data } from "autoprefixer";

export const ProductContext = createContext();

const ProductContextProvider = ({children}) => {
    const [query, setQuery] = useState({
        isLoading: true,
        data : null
    })
    const [product, dispatch] = useReducer(productReducer, initialProduct)
    useEffect(()=>{
        const fetch = async () =>{
            await axios.get('http://localhost:3001/products').then(res =>{
                setQuery({data: res.data.products, isLoading: false})
            })
        }
        fetch();
    },[])
    return <ProductContext.Provider value={{query, product, dispatch}}>{children}</ProductContext.Provider>
}

export default ProductContextProvider
