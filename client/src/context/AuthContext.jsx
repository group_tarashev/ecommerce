import axios from "axios";
import { createContext, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
export const AuthContext = createContext();

const getCookie = () =>{
    const userC = Cookies.get('user');
    let user = null;
    if(userC !== undefined){
        user = JSON.parse(userC)
    }else{
        user = null
    }
    return user;
}

const AutContextProvider = ({ children }) => {
  const [user, setUser] = useState({
    message: '',
    data: getCookie(),
  });
  const getUser = async (user) => {
    await axios.post("http://localhost:3001/login", user).then((res)=>{
        if(typeof res.data == 'string'){
            setUser({message: res.data})
        }else{
            setUser({
                message: 'Success',
                data: res.data
            })
            // navigate('/user');
        }
        return res.data
    });
  };
  return <AuthContext.Provider value={{getUser, setUser, user}}>{children}</AuthContext.Provider>;
};

export default AutContextProvider
