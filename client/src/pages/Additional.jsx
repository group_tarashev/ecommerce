import React, { useState } from "react";
import Input from "../components/Input";
import axios from "axios";

const Additional = () => {
  const [input, setInput] = useState({
    id: "",
    title: "",
    desc: "",
    category:'',
    act_price: '',
    sizes: [],
    images: [],
  });
  const [sent, setSent] = useState(false);
  const [check, setCheck] = useState(true);
  const [sizes, setSizes] = useState();
  const [sizeInput, setSizeInput] = useState([]);
  const [image, setImage] = useState([])

  const handleInput = (e) => {
    const { value, name } = e.target;
    const inputs = { [name]: value };
    if (
      input.desc.length > 0 &&
      input.id.length > 0 &&
      input.title.length > 0
    ) {
      setCheck(false);
    }
    setInput((prev) => ({ ...prev, ...inputs }));
    // console.log(name, value)
  };

  
  
  const handleSizeInput = (e, size) =>{
    e.preventDefault();
    console.log(e.target.children[1].value);
    const {value, name} = e.target.children[1]
    console.log(value, name)
    let sizeObj = {[name]: value, q: size};
    if(name == 'q'){
        return
        
    }
    setInput(prev =>({
        ...prev, 
        sizes: [...prev.sizes, sizeObj]
    }))
  }
  const handleSubmitImage = (e) =>{
    console.log(e)
    setInput(prev =>({
      ...prev,
      images: [...prev.images, e.target.value]
    }))
  }

  const handlePost = async () => {
    await axios.post("http://localhost:3001/addproduct", input);
    console.log(input);
    setSent(true);
    setCheck(true);
  };
  if (sent) {
    return (
      <h2>
        Added to Json {"  "}{" "}
        <button onClick={() => setSent(false)}>Go back</button>
      </h2>
    );
  }
  const inpSize = (<div className="flex w-40 mt-2 gap-4" key={Math.random()}>
    <Input class={"w-20"} name={"eu "} id={"eu"} handleInput={()=>{}} handleSubmit={handleSizeInput}  />
  </div>
  );
  const inpImage = (
    <div key={Math.random()}><input name={'images'} id={'image'} onChange={handleSubmitImage} placeholder="paste link to image" className="border-2 w-full mt-3"/></div>
  )
  const handleSize = () => {
    setSizeInput((prev) => [...prev, inpSize]);
  };
  const handleImage = () =>{
    setImage(prev => [...prev, inpImage])
  }

  const sizeContent = (
    <div>
      {sizeInput}
      <button className="border-2 mt-3 p-2" onClick={() => handleSize()}>Add sizes</button>
    </div>
  );
  const imageContent = (
    <div>
      {image}
      <button className="border-2 mt-3 p-2" onClick={() => handleImage()}>Add images</button>
    </div>
  )
  return (
    <div className="mt-10 flex flex-col gap-2">
      {inputs.map((item, i) => (
        <Input
          key={i}
          id={item.id}
          name={item.name}
          input={input}
          handleInput={handleInput}
        />
      ))}
      <hr />
      {sizeContent}
      {imageContent}
      <button className="border-2" disabled={check} onClick={handlePost}>
        Add to Json
      </button>
    </div>
  );
};

export default Additional;

const inputs = [
  {
    id: "id",
    name: "ID",
  },
  {
    id: "title",
    name: "Title",
  },
  {
    id: "desc",
    name: "desc",
  },
  {
    id: "price",
    name: "price",
  },
  {
    id: "discount",
    name: "discount",
  },
  {
    id: "act_price",
    name: "Actual Price",
  },
  {
    id:'category',
    name: 'Category',
  }
];
const sizes = [
  {
    eu: [37, 36, 38, 39, 40, 41, 42, 43, 44, 45],
    uk: [4, 5, 6, 7, 8, 9, 10, 11],
    us: [3, 4, 5, 6, 7, 8, 9, 10],
  },
];
