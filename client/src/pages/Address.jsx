import React, { useContext, useState } from "react";
import Input from "../components/Input";
import { ProductContext } from "../context/ProductContext";
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";
import InputAddress from "../components/InputAddress";

export const prop = [
  {
    id: "firstname",
    name: "First Name",
  },
  {
    id: "lastname",
    name: "Last Name",
  },
  {
    id: "region",
    name: "Region",
  },
  {
    id: "city",
    name: "City",
  },
  {
    id: "phone",
    name: "Phone Number",
  },
];

const Address = () => {
  const [input, setInput] = useState({deliver: 'speedy'});
    const {product, dispatch} = useContext(ProductContext)
    const navigate = useNavigate();
    const totalPrice = product.reduce((a,b) =>{
        return a + parseInt(b.price) * b.count
    },0)
  const handleInput = (e) => {
    const { name, value } = e.target;
    const inputs = { [name]: value };
    setInput((prev) => ({ ...prev, ...inputs }));
};
const handleSelect = (e) =>{
    console.log(e.target.value)
    setInput(prev => ({...prev, deliver: e.target.value}))
}
const finish = () =>{
    dispatch({
        type: 'clear',
    })
    Cookies.set('cart', JSON.stringify([]),{expires:1, path:'/', sameSite:'None'})
    navigate('/confirm');
}
return (
    <div className="xl:mt-20 lg:mt-20 md:mt-20 sm:mt-5 flex flex-col items-center gap-5">
      <h2 className="mb-10">Address to ship</h2>
      <InputAddress data={prop} handleInput={handleInput}/>
      <select name="delivery" id="delivery" onChange={handleSelect}>
        <option value="speedy">Speedy</option>
        <option value="econt">Econt</option>
      </select>
      <div><h2>Total price: ${totalPrice} + {input?.deliver === 'speedy' ?  5.20 : 5.00}</h2></div>
      <button onClick={() =>{finish()}} className="bg-orange-500 p-2 text-slate-50">Purchese</button>
    </div>
  );
};

export default Address;
