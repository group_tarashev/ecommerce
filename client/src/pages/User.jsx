import React, { useContext, useEffect, useState } from "react";
import { AuthContext } from "../context/AuthContext";
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";

import Login from "../components/Login";
import Sign from "../components/Sign";
import axios from "axios";
import { setUserCookie } from "../cookies/user";
import Address from "../components/Address";
const User = () => {
  const { user, setUser } = useContext(AuthContext);
  const [change, setChange] = useState(false);
  const [image, setImage] = useState("");
  const [address, setAddress] = useState(false)
  const navigate = useNavigate();
  const handleChangeImage = () => {
    const { email } = user?.data;
    axios.post("http://localhost:3001/editauth", { email, image }).then(() => {
      const str = Cookies.get("user");
      let obj = JSON.parse(str);
      obj.image = image;
      Cookies.set("user", JSON.stringify(obj), {
        expires: 7,
        path: "/",
        sameSite: "Strict",
      });

      window.location.reload();
    });
  };
  if (user.data !== null && user.data !== undefined) {
    const { image, username, points } = user?.data;
    setUserCookie(user);
    return (
      <div className="flex flex-col gap-20 items-center mt-20">
        <div className="flex items-center gap-10 sm:flex-col">
          <div className="flex flex-col items-center gap-4">
            <img
              className="w-20 h-20 object-cover rounded-full"
              src={image}
              alt=""
            />
            <button
              onClick={() => setChange(!change)}
              className="bg-green-200 p-2"
            >
              Change Image
            </button>
            {change && (
              <div>
                {" "}
                <input
                  onChange={(e) => setImage(e.target.value)}
                  className="border-2 pl-2"
                  placeholder="paste link here"
                />
                <button
                  onClick={() => handleChangeImage()}
                  className="border-2"
                >
                  Change
                </button>
              </div>
            )}
          </div>
          <div className="flex gap-5 items-center">
            <h2 className="bg-orange-500 p-2 rounded-md text-slate-50">{username}</h2>
            <h2>points: {points}</h2>
          </div>
          
        </div>
        {address && <Address setAddress={setAddress}/>}
        <button onClick={() => setAddress(!address)} className="bg-green-200 p-2">Add your address</button>
        <div className="flex gap-20">
          {/* <button className="p-2 bg-green-400 rounded-lg text-slate-50 w-20">Edit</button> */}
          <button
            className="bg-red-700 text-blue-50 rounded-lg p-2"
            onClick={() => {
              setUser({ data: null, message: "" });
              navigate("/");
              Cookies.remove("user");
            }}
          >
            Log Out
          </button>
        </div>
      </div>
    );
  }

  return (
    <div className="flex flex-col mt-20 justify-center items-center mb-20">
      <h2 className="mb-10 text-2xl">Sign in</h2>
      <div className="felx mb-10 ">
        <Sign />
      </div>
      <h2 className="mb-20 text-2xl">Log in</h2>
      <div>
        <Login />
      </div>
    </div>
  );
};

export default User;
