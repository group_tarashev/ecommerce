import React from 'react'
import { useNavigate } from 'react-router-dom'

const Confirmation = () => {
    const navigate = useNavigate();
  return (
    <div className='w-full flex flex-col items-center justify-center mt-20 bg-slate-300 p-4'>
        <h2 className='self-center'>Our staff will connect with you for confirmation</h2>
        <button className='p-2 bg-orange-500 text-slate-50 rounded-lg' onClick={() => navigate('/')}>Back to site</button>
    </div>
  )
}

export default Confirmation