import React, { useContext } from 'react'
import { ProductContext } from '../context/ProductContext'
import Card from '../components/Card';
import CardCartLg from '../components/CardCartLg';
import { useNavigate } from 'react-router-dom';

const Checkout = () => {
    const {product} = useContext(ProductContext);
    const navigate = useNavigate();
    const totalPrice = product.reduce((a,b) =>{
        return a + parseInt(b.price) * b.count
    },0)
    if(product.length == 0){
        return <h1 className='mt-20'>Empty Cart</h1>
    }
  return (
    <div className='w-1/2 mt-20'>
       {product.map((item) =>(
         <CardCartLg key={item.id} product={item}/>
       ))}
       <div className='flex justify-between'>
        <h2>Total: ${totalPrice}</h2>
        <button onClick={()=> navigate('/address')} className='bg-orange-400 p-2 rounded-sm text-slate-50'>Order</button>
       </div>
    </div>
  )
}

export default Checkout