import React from 'react'
import Category from '../components/Category'

const Women = () => {
  return (
    <Category str={'women'}/>
  )
}

export default Women