import React from 'react'

const NotFound = () => {
  return (
    <div className='absolute top-1/2 left-1/2 grid place-content-center'>Page not found</div>
  )
}

export default NotFound