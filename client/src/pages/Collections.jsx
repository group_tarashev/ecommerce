import React, { useContext } from 'react'
import { ProductContext as ProductContext } from '../context/ProductContext'
import Card from '../components/Card'
import { useNavigate } from 'react-router-dom'

const Collections = () => {
    const {query} = useContext(ProductContext)
    if(query.isLoading){
        return <h2>Loading</h2>
    }
  return (
    <div className='flex w-full gap-20 p-20 flex-wrap justify-center sm:gap-3 sm:p-5'>
        {query?.data.map((item, i) =>(
            <Card key={item.id} item={item} />
        ))}
    </div>
  )
}

export default Collections