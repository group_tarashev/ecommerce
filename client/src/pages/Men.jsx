import React, { useContext } from 'react'
import { ProductContext } from '../context/ProductContext'
import Card from '../components/Card';
import Category from '../components/Category';

const Men = () => {
 
  return (
    <Category str={'men'}/>
  )
}

export default Men