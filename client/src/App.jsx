import React, { useContext } from "react";
import "./App.css";
import Nav from "./components/Nav";
import Product from "./components/Product";
import {
  Route,
  Routes,
} from "react-router-dom";
import Collections from "./pages/Collections";
import Women from "./pages/Women";
import Men from "./pages/Men";
import About from "./pages/About";
import Contact from "./pages/Contact";
import Checkout from "./pages/Checkout";
import Additional from "./pages/Additional";
import User from "./pages/User";
import { AuthContext } from "./context/AuthContext";
import NotFound from "./pages/NotFound";
import Address from "./pages/Address";
import Confirmation from "./pages/Confirmation";

const App = () => {
  const {user} = useContext(AuthContext); 
  return (
    <div className="max-w-9xl  flex flex-col items-center sm:w-full sm:overflow-hidden">
      <Nav />
      <Routes>
        <Route path="/" element={<Collections />} />
        <Route path="/men" element={<Men />} />
        <Route path="/women" element={<Women />} />
        <Route path="about" element={<About />} />
        <Route path="/contact" element={<Contact />} />
        <Route path="/product/:id" element={<Product />}/>
        <Route path="/checkout" element={<Checkout/>}/>
        <Route path="/user" element={<User/>}/>
        {user !== null && user.data?.isAdmin && <Route path="/additional" element={<Additional/>}/>}
        <Route path="/address" element={<Address/>}/>
        <Route path="/confirm" element={<Confirmation/>}/>
        <Route path="*" element={<NotFound/>}/>
      </Routes>
    </div>
  );
};
export default App;
