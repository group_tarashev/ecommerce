import React from 'react'
import Input from './Input'

const InputAddress = ({data, handleInput}) => {
  return (
    <ul className="flex flex-col gap-3">
        {data.map((item, i) => (
          <li key={i}>
            <Input id={item.id} name={item.name} handleInput={handleInput} class={'border-b-4'}/>
          </li>
        ))}
      </ul>
  )
}

export default InputAddress