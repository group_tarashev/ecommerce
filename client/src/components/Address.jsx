import React, { useState } from "react";
import Input from "./Input";
import InputAddress from "./InputAddress";
import { prop } from "../pages/Address";

const Address = ({setAddress}) => {
  const [input, setInput] = useState();
  const handleInput = (e) => {
    const {name, value} = e.target;
    const inputs = {[name]: value};
    setInput(prev => ({...prev, ...inputs}))
  };
  return (
    <div className="flex flex-col items-center">
      <InputAddress data={prop} handleInput={handleInput} />
      <div className="flex gap-10 items-center mt-5 ">
        <button className="bg-green-200 p-2 rounded-lg w-20">Add</button>
        <button onClick={() => setAddress(false)} className=" p-2 bg-red-400 rounded-lg w-20">Cancel</button>
      </div>
    </div>
  );
};

export default Address;
