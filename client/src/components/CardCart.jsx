import React, { useContext } from "react";
import { ProductContext } from "../context/ProductContext";

const CardCart = ({ product }) => {
    const {dispatch} = useContext(ProductContext);
    const handleDelete = (id) =>{
        dispatch({
            type: 'remove',
            id: id
        })
    }
  return (
    <li key={product.id} className="flex items-center p-4 bg-slate-50 mb-3 rounded-lg justify-between w-full">
        <img draggable={false} className="w-12 h-12" src={product.image} alt="" />
      <div className="flex-col">
        <h5 className="text-sm">{product.title}</h5>
        <span className="text-sm">${product.price} x {product.count}{" "} </span>
        <span className="text-sm font-black">${parseInt(product.price) * product.count}</span>
      </div>
      <img onClick={() => handleDelete(product.id)} src="../images/icon-delete.svg" alt="del" />
    </li>
  );
};

export default CardCart;
