import React, { useContext } from "react";
import { ProductContext } from "../context/ProductContext";

const CardCartLg = ({ product }) => {
  const { dispatch } = useContext(ProductContext);
  const handleDelete = (id) => {
    dispatch({
      type: "remove",
      id: id,
    });
  };
  if (product.length == 0) {
    return <h2>Cart is empty</h2>;
  }
  return (
    <li
      key={product.id}
      className="flex items-center p-4 bg-slate-100 mb-3 rounded-lg justify-between w-full shadow-xl sm:flex-col"
    >
      <img className="w-32 h-32 object-cover" src={product.image} alt="" />
      <div className="">Size: {product.sizes}</div>
      <div className="flex-col">
        <h5 className="text-sm">{product.title}</h5>
        <span className="text-sm">
          ${product.price} x {product.count}{" "}
        </span>
        <span className="text-sm font-black">
          ${parseInt(product.price) * product.count}
        </span>
      </div>
      <img
        onClick={() => handleDelete(product.id)}
        src="./images/icon-delete.svg"
        alt="del"
      />
    </li>
  );
};

export default CardCartLg;
