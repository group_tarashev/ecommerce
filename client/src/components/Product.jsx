import React, { useContext, useEffect, useRef, useState } from "react";
import CardContent from "./CardContent";
import { ProductContext } from "../context/ProductContext";
import Carousel from "./Carousel";
import { useLocation } from "react-router-dom";
import Modal from "./Modal";

const Product = () => {
    const {query} = useContext(ProductContext)
    const {state} = useLocation();
    const [modal, setModal] = useState(false);
    const handleModal  = () =>{
      setModal(!modal)
    }
    
    if(query.isLoading){
      return <h1 className="absolute translate-x-1/2 translate-y-1/2 top-1/2 left-1/2">Loading ... </h1>
    }
    
  return (
    <div className="flex items-center p-20 gap-10 md:p-10 md:gap-2 flex-wrap md:justify-center sm:justify-center sm:p-0" id="product">
      <Carousel handleModal={handleModal} images={state.images}/>
      <div className="right-side">
        <CardContent data={state}/>
      </div>
      <Modal modal={modal} handleModal={handleModal}><Carousel images={state.images} modal={modal}/></Modal>
    </div>
  );
};

export default Product;
