import React, { useContext, useEffect, useState } from "react";
import { ProductContext } from "../context/ProductContext";
import CardCart from "./CardCart";
import { useNavigate } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";
import { setProduct } from "../cookies/products";
const nav = ["Collections", "Men", "Women", "About", "Contact"];
const Nav = () => {
  const [index, setIndex] = useState(0);
  const [openCart, setOpenCart] = useState(false);
  const [openMenu, setOpenMenu] = useState(false);
  const handleSelet = (i) => {
    setIndex(i);
  };
  const handleCart = () => {
    setOpenCart(!openCart);
  };
  const { product } = useContext(ProductContext);
  const {user} = useContext(AuthContext)
  // console.log(product)
  // const { isLoading, data } = useContext(ProductContext).query;
  const path = ['/','/men','/women', '/about', '/contact']
  const navigate = useNavigate();
  useEffect(() =>{
    setProduct(product); //cookies
  },[product])
  return (
    <div className="nav flex w-full items-center justify-between px-20  border-b-2  sm:w-screen sm:px-8 sm:justify-between sm: h-20">
      <div className="flex items-center gap-20 md:gap-10">
        <h1 className="font-bold tracking-wider text-2xl md:text-xl cursor-pointer sm:text-sm" onClick={() => navigate('/')}>Sneakers</h1>
        <ul className={openMenu ? "menu-active flex gap-5" : "menu flex gap-5 "}>
          <button onClick={() => setOpenMenu(false)} className="hidden sm:block absolute right-5 border-2 p-1 border-orange-600 text-orange-600 w-10 h-10 text-lg mt-4">X</button>
          {nav.map((item, i) => {
            return (
            <li
              key={i}
              onClick={() => {handleSelet(i); navigate(path[i]); setOpenMenu(false)}}
              className={
                index === i
                  ? "cursor-pointer py-5 border-b-4 border-amber-400 md:py-2 md:text-sm sm:text-xl"
                  : "cursor-pointer py-5 md:py-2 md:text-sm sm:text-xl"
              }
            >
              {item}
            </li>
          )})}
          {user?.data?.isAdmin && <li onClick={() => navigate('/additional')} className="py-5 cursor-pointer md:py-2">Admin</li>}
        </ul>
      </div>
      <div className="avatar flex items-center gap-10 relative">
        <div className="cart cursor-pointer z-10">
          {product.length > 0 && (
            <div className="absolute bottom-6 left-6 bg-red-600 flex items-center justify-center rounded-xl text-slate-50 w-5 h-5 text-sm">
              {product.length}
            </div>
          )}
          <img onClick={handleCart} src="../images/icon-cart.svg" alt="cart" />
          {openCart && (
            <div className="absolute w-72 h-auto bg-slate-100 top-12 right-0 shadow-xl flex-col items-center justify-center max-h-72 overflow-auto sm:right-[-70px]">
              <div className="p-2 pb-4 border-b-2 flex justify-between">
              <h2 className="border-stone-300 p-2">Cart</h2>
              <button onClick={handleCart} className="text-sm px-3 bg-orange-500 text-slate-50 rounded-lg">Close</button>
              </div>
              {product.length === 0 ? (
                <p className="my-10 ml-16">
                  Your cart is empty
                </p>
              ) : (
                <ul className="flex-col p-2 items-center justify-center w-full">
                  {product.map((item, i) => (
                    <CardCart key={new Date().getTime()} product={item} />
                  ))}
                  <button onClick={() => {navigate('/checkout'); handleCart()}} className=" text-slate-50 h-12 rounded-lg bg-orange-500 w-full">
                    Check out
                  </button>
                </ul>
              )}
            </div>
          )}
        </div>
        <div className="avatar-img cursor-pointer" onClick={() => navigate('/user')}>
          {user?.data === null ? <h2>Sign In</h2> : <img src={user?.data?.image} alt="A" className="w-10 h-10 object-cover rounded-full md:w-7 md:h-7" />}
        </div>
      </div>
      <button onClick={() => setOpenMenu(true)} className="hidden sm:flex bg-orange-600 p-2 text-slate-50 rounded-lg sm:text-sm">MENU</button>
    </div>
  );
};

export default Nav;
