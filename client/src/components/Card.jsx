import React from "react";
import { useNavigate } from "react-router-dom";

const Card = ({ item }) => {
    const navigate = useNavigate();

  return (
    <div className="w-60 h-76 rounded-lg  cursor-pointer overflow-hidden  bg-slate-200 z-0" onClick={() => navigate(`/product/:${item.id}`,{state: item})}>
      <img
        className="w-full h-52  object-cover "
        src={item.images[0]}
        alt={item.title}
        draggable={false}
      />
      <div className="p-2 bg-slate-200 flex flex-col h-28 justify-between relative">
        <div className="flex justify-between ">
          <h2>{item.title}</h2>
          <p className="bg-orange-200 h-8 p-1">-{item.discount}%</p>
        </div>
        <div className="flex justify-between ">
            <span className="absolute left-3 text-red-500">-------</span>
          <h2>${item.act_price}</h2>
          <h2>${item.price}</h2>
        </div>
      </div>
    </div>
  );
};

export default Card;
