import React, { useContext, useEffect, useState } from "react";
import InputUser from "./InputUser";
import {
  LoadCanvasTemplate,
  loadCaptchaEnginge,
  validateCaptcha,
} from "react-simple-captcha";
import { AuthContext } from "../context/AuthContext";
import Cookies from "js-cookie";

const Login = () => {
  const { getUser, user, setUser } = useContext(AuthContext);
  const [userLogin, setUserLogin] = useState({
    email: "",
    password: "",
  });

  const handleLogin = (e) => {
    const { name, value } = e.target;
    const input = { [name]: value };
    setUserLogin((prev) => ({
      ...prev,
      ...input,
    }));
  };
  const obj = Cookies.get("user");
  if (obj !== undefined) {
    // const objPars = JSON.parse(obj)
    console.log(obj);
  }
  const handleLoginSubmit = async (e) => {
    e.preventDefault();
    await getUser(userLogin);
  };
  return (
    <form className="flex flex-col gap-2 w-full" onSubmit={handleLoginSubmit}>
      {login.map((item, i) => (
        <InputUser
          key={i}
          id={item.id}
          name={item.name}
          type={item.type}
          label={item.label}
          min={item.min}
          handleInput={handleLogin}
        />
      ))}

      {user && <h2 className="self-center text-red-500">{user.message}</h2>}
      <button className="mt-2 border-2">Log In</button>
    </form>
  );
};
const login = [
  {
    id: "email",
    name: "email",
    type: "email",
    label: "Email",
    min: 6,
  },
  {
    id: "password",
    name: "password",
    type: "password",
    label: "Password",
    min: 6,
  },
];
export default Login;
