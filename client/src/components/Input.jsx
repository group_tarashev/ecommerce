import React, { useState } from "react";

const Input = ({ name, id, input, ...props }) => {
  const [size, setSize] = useState(0);
  const [show, setShow] = useState(false);
  const handleInputSize = (e) => {
    setSize(e.target.value);
  };
  return (
    <form
      className="flex space-between w-96 gap-2 items-center sm:flex-col sm:w-full"
      onSubmit={(e) => props.handleSubmit?.(e, size)}
    >
      <label htmlFor={id} className="w-full sm:w-32 sm:self-start">
        {name}
      </label>
      <input
        required={true}
        className={"border-2 " + props.class}
        type="text"
        name={id}
        id={id}
        onChange={(e) => props.handleInput(e)}
      />
      {props.handleSubmit && (
        <input
          className="border-2 w-20"
          placeholder="size"
          onChange={handleInputSize}
        />
      )}
      {props.handleSubmit && (
        <button type="submit" onClick={() => setShow(true)}>
          {show || "sub"}
        </button>
      )}
      {show && <p className="text-green-400"> Added</p>}
    </form>
  );
};

export default Input;
