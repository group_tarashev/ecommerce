import React from "react";
import Carousel from "./Carousel";

const Modal = ({ modal, handleModal , children}) => {
  return (
    <>
      {modal && (
        <div
          className="absolute w-full h-full flex-col grid place-content-center left-0 top-0 z-20 sm:w-screen sm:h-screen"
          style={{ background: "rgba(0,0,0,0.7)" }}
        >
          <div className="relative p-4">
            <button className="absolute top-0 right-10 md:right-10" onClick={handleModal}>
              <img className="w-6 h-6" src="../images/icon-close.svg" draggable={false} alt="close" />
            </button>
            {children}
          </div>
        </div>
      )}
    </>
  );
};

export default Modal;
