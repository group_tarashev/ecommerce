import React, { useContext, useEffect, useMemo, useState } from 'react'
import { ProductContext } from '../context/ProductContext'
const Carousel = ({handleModal, images, modal}) => {
    const [index, setIndex] = useState(0);
  const handleIndex = (i) => {
    setIndex(i);
  };
   const handlePrev = () =>{
    setIndex(prev => prev > 0 ? prev -1 : images.length - 1)
   }
   const handleNext = () =>{
    setIndex(prev => prev < images.length -1 ? prev + 1 : 0)
   }
  return (
    <div className='relative flex flex-col sm:items-center'>
      {modal && <button draggable={false} onClick={handlePrev} className='bg-stone-50 p-2 rounded-full w-10 h-10 flex items-center justify-center absolute top-40 left-5'>
        <img src="../images/icon-previous.svg" alt="" /></button>}
        { images.map((item, i) => {
           return (
          <React.Fragment key={i}>
            <img draggable={false} onClick={handleModal} loading='lazy'className={ index == i ? "h-96 w-96 rounded-xl object-cover m-10 sm:h-64 sm:w-64": 'hidden'} src={item} alt="sneakers" />
          </React.Fragment>
        )})}
        <ul className="flex gap-10 w-96 my-4 m-10 sm:m-2 sm:gap-2 sm:w-64">{
             images.map((item, i) =>(
                <li  onClick={() => handleIndex(i)}  key={i}><img draggable={false}  src={item} loading='lazy' alt="" className="rounded-lg cursor-pointer object-cover w-20 h-20"/></li>
            ))
            }</ul>
          {modal && <button draggable={false} onClick={handleNext} className='bg-stone-50 p-2 rounded-full w-10 h-10 flex items-center justify-center absolute top-40 right-5'>
              <img src="../images/icon-next.svg" alt="" />
            </button>}
      </div>
  )
}

export default Carousel