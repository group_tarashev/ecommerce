import React from 'react'

const Notice = () => {
  return (
    <div className='absolute top-[-70px] bg-orange-600 p-4 text-slate-50 text-lg'>Added to cart</div>
  )
}

export default Notice