import React from 'react'

const InputUser = ({handleInput, label, id, name, type, min}) => {
  return (
    <div className=" flex justify-between w-196 gap-2">
            <label  htmlFor={id}>
              {label}
            </label>
            <input
              className="border-2 pl-2"
              type={type}
              id={id}
              name={name}
              required={true}
              onChange={handleInput}
              minLength={min}
              
            />
          </div>
  )
}

export default InputUser