import React, { useContext, useState } from "react";
import { ProductContext } from "../context/ProductContext";
import Input from "./Input";
import Notice from "./Notice";

const CardContent = ({ data }) => {
  const { dispatch } = useContext(ProductContext);
  const [count, setCount] = useState(1);
  const [select, setSelect] = useState("eu");
  const [activeSize, setActiveSize] = useState(null);
  const [selectSize, setSelectSize] = useState({});
  const [notice, setNotice] = useState(false);
  const handleAdd = (data) => {
    dispatch({
      type: "added",
      id: data.id + activeSize,
      desc: data.desc,
      title: data.title,
      price: data.price,
      count: count,
      image: data.images[0],
      size: selectSize,
    });
  };
  const handleCountIncrease = () => {
    setCount((prev) => prev + 1);
  };
  const handleCountDecrease = () => {
    if (count > 1) {
      setCount((prev) => prev - 1);
    }
  };
  const handleSelect = (e) => {
    setSelect(e.target.value)
  };
  const handleActiveSize = (item)=>{
    setActiveSize(item)
    setSelectSize(Object.keys(item))
  }
  const handleNotice = () =>{
    setNotice(true);
    const time = setTimeout(()=>{
      setNotice(false)
    },2000)
    return () => clearTimeout(time);
  }
  return (
    <div className="sm:w-screen p-2 flex-col ">
      <h3 className="">Sneakers compnay</h3>
      <h1 className="text-5xl my-5 sm:text-3xl">{data.title}</h1>
      <p className="w-96 sm:w-full">{data.desc}</p>
      <div className="flex gap-10 mt-10 items-center">
        <span className="text-2xl">${data.price}</span>
        <span className="bg-orange-100 text-orange-400 p-2 rounded-lg">
          {data.discount}%
        </span>
      </div>
      <span className="text-slate-400">${data.act_price}</span>
      <select
        className="flex gap-2 my-5 p-1"
        onChange={handleSelect}
      >
        <option value="eu">EU</option>
      </select>
      <form className="flex gap-3 flex-wrap">
        {
            data.sizes.map((item, i) =>{
              return(
                <input key={i} onClick={() => {handleActiveSize(item); setSelectSize(item)}} type="button" value={item.eu} className={activeSize == item ?"border p-2 border-orange-500 cursor-pointer" :"p-2 border cursor-pointer"}/>
            )})
        }
      </form>
      <div className="flex items-center gap-10 my-6">
        <div className="flex gap-2 bg-slate-200  p-3 w-32 justify-between rounded-lg">
          <button
            onClick={handleCountDecrease}
            className="text-orange-400 text-2xl"
          >
            -
          </button>
          <span className="text-2xl">{count}</span>
          <button
            onClick={handleCountIncrease}
            className="text-orange-400 text-2xl"
          >
            +
          </button>
        </div>
        { notice && <div className="absolute bottom-[-150px] right-0 w-72 h-20">
          <Notice/>
        </div>}
        <button
          onClick={() => {handleAdd(data); handleNotice()}}
          disabled={activeSize == null}
          className=" p-4 px-10 flex gap-4 bg-orange-400 rounded-lg text-slate-100"
        >
           <span>Add to cart</span>
        </button>
      </div>
    </div>
  );
};

export default CardContent;
