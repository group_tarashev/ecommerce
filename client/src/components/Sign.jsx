import React, { useEffect, useState } from 'react'
import InputUser from './InputUser';
import { LoadCanvasTemplate, loadCaptchaEnginge, validateCaptcha } from 'react-simple-captcha';
import axios from 'axios';

const Sign = () => {
    const [captcha, setCaptcha] = useState("");
    const [input, setInput] = useState({
        username: "",
        password: "",
        re_password: "",
        image: "",
      });
  const [userAdd, setUserAdd] = useState("");

      const [notice, setNotice] = useState(<></>);
  useEffect(() => {
    loadCaptchaEnginge(6,'blue', 'white');
  }, []);
  const handleInput = (e) => {
    const { name, value } = e.target;
    // console.log(name, value)
    const set = { [name]: value };
    if (set.password == set.re_password) {
      setNotice(<></>);
    }
    setInput((prev) => ({
      ...prev,
      ...set,
    }));
  };
  const handleSubmit = async (e) => {
    e.preventDefault();
    if(validateCaptcha(captcha) !== true){
        setNotice('Wrong captcha')
        return ;
    }
    if (input.password !== input.re_password) {
      setNotice(<p>Password Should Match</p>);
      return;
    }
    await axios.post("http://localhost:3001/users", { input }).then((res) => {
      setUserAdd(res.data);
    });
    setInput({
      username: "",
      password: "",
      re_password: "",
      image: "",
    });
    setCaptcha('')
    // console.log(input);
  };
  return (
    <form className="flex flex-col gap-2 w-full" onSubmit={handleSubmit}>
    {userInput.map((item, i) => (
        <InputUser
        key={i}
        handleInput={handleInput}
        id={item.id}
        name={item.name}
        type={item.type}
        label={item.label}
        min={item.min}
        />
        ))}
    <h2 className='self-center text-red-500'>{notice}</h2>
    <div className='self-center'>

    <LoadCanvasTemplate />
    <input className='border-2' placeholder='Captcha' type="text" onChange={(e) => setCaptcha(e.target.value)} />
    </div>
    {<h2 className="text-green-500 self-center">{userAdd}</h2>}
    {userAdd.length > 0 || <button  className="border-2">Sign in</button>}
  </form>
  )
}

export default Sign

const userInput = [
    {
      id: "username",
      name: "username",
      type: "text",
      label: "Name",
      min: 6,
    },
    {
      id: "email",
      name: "email",
      type: "email",
      label: " Email",
      min: 6,
    },
    {
      id: "password",
      name: "password",
      type: "password",
      label: "Password",
      min: 6,
    },
    {
      id: "re_password",
      name: "re_password",
      type: "password",
      label: "Re-Password",
      min: 6,
    },
    {
      id: "image",
      name: "image",
      type: "text",
      label: "Image Link",
    },
  ];
  