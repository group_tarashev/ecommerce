import React, { useContext } from 'react'
import { ProductContext } from '../context/ProductContext'
import Card from '../components/Card';

const Category = ({str}) => {
  const {query} = useContext(ProductContext);
  
  
  if(query.isLoading){
    return <h1>Loading...</h1>
  }
  const category = query.data?.filter((item => {
    return item.category === str
  }))

  return (
    <div className='flex w-full gap-20 p-20 justify-center flex-wrap'>{
      category?.map((item) =>(
        <Card item={item} key={item.id}/>
      ))
      }</div>
  )
}

export default Category