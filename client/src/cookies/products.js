import Cookies from 'js-cookie'

export const getProduct = () =>{

    const json = Cookies?.get('cart');
    if(json){
        return JSON.parse(json)
    }else{
        return []
    }
}

export const setProduct = (product) =>{
    Cookies.set('cart', JSON.stringify(product), {expires: 1, path: '/', sameSite:'None'})
}