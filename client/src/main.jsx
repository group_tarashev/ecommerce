import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";
import "./index.css";
import ProductContextProvider from "./context/ProductContext.jsx";
import { BrowserRouter } from "react-router-dom";
import AutContextProvider from "./context/AuthContext.jsx";

ReactDOM.createRoot(document.getElementById("root")).render(
  <BrowserRouter>
    <ProductContextProvider>
      <AutContextProvider>
        <App />
      </AutContextProvider>
    </ProductContextProvider>
  </BrowserRouter>
);
