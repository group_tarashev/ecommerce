const fs = require('fs')
const product = (req, res, next) =>{
    try {
        let productJs = fs.readFileSync('./data/product.json', 'utf-8')
        productJs = JSON.parse(productJs)
        if(productJs){
            res.json(productJs);
        }else{
            res.status(404).send("File missing")
        }
    } catch (error) {
        console.log(error)
    }
    next();
}

module.exports = product