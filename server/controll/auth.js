const uuid = require('uuid')
const fs = require('fs')
const bcrypt = require('bcrypt')
const auth = (req, res) =>{
    const {username, password, email, image} = req.body.input
    const users =  fs.readFileSync('./data/users.json', 'utf-8')
    const userJS = JSON.parse(users)
    const isExisting = userJS.users.find(item => item.email === email);
    // console.log(uuid.v4())
    if(isExisting){
        res.send("User Already exist")
        return console.log("User Already exist")
    }else{
        const salt = bcrypt.genSaltSync(10);
        const hash = bcrypt.hashSync(password, salt)
        userJS.users.push({
            userId: uuid.v4(),
            userName: username,
            password: hash,
            email : email,
            image: image,
            isAdmin: false,
            points: 0
        })
        fs.writeFileSync('./data/users.json', JSON.stringify(userJS))
        res.send("New user has been added")
    }
    // res.json(JSON.parse(users))
}

module.exports = auth