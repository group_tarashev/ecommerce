const fs = require('fs');

const addProduct = (req, res, next) =>{
    const {id, title, desc, price, discount, act_price, category, sizes, images} = req.body

    let productJs = fs.readFileSync('./data/product.json', 'utf-8')
        productJs = JSON.parse(productJs)
    const addProd = {
        id: Number(id),
        title: title,
        desc: desc,
        price: price,
        discount: discount,
        category: category,
        act_price : act_price,
        images: images,
        sizes: sizes
    }

    const arr = Array.from(productJs.products)
    arr.push(addProd)
    let object = {products: arr}
    // console.log(object)
    fs.writeFileSync('./data/product.json', JSON.stringify(object))
    res.send('Added')
    next();
}

module.exports = addProduct