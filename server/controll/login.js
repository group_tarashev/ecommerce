const fs = require('fs');
const bcrypt = require('bcrypt')
const login = (req, res) =>{
    const {email, password} = req.body
    const users = fs.readFileSync('./data/users.json', 'utf8');
    const userJs = JSON.parse(users);

    const isExist = userJs.users.find(item => item.email == email)
    if(isExist){
        console.log(isExist)
        console.log(req.body)
        const pass = bcrypt.compareSync(password, isExist.password)
        if(pass){
            res.send({
                username: isExist.userName,
                image: isExist.image,
                points: isExist.points,
                isAdmin: isExist.isAdmin,
                email: isExist.email
            })
        }else{
            res.send("Wrong password or email")
        }
    }else{
        res.send("User not found")
    }
}

module.exports = login