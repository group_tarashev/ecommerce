const fs = require('fs')
const editAuth = (req, res) =>{
    const {email, image} = req.body
    const userJson = fs.readFileSync('./data/users.json', 'utf8');
    const user = JSON.parse(userJson);
    const isExist = user.users.find(item => item.email === email);
    if(isExist){
        user.users.map((item) =>{
            if(item.email == email){
                return item.image = image
            }
        })
        fs.writeFileSync('./data/users.json', JSON.stringify(user))
        res.send("Image has been updated")
    }else{
        res.send("User does not exist")
    }
    // console.log(req.body)
    // res.send("Add image")
}

module.exports = editAuth