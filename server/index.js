const express = require('express')
const cors = require('cors');
const app = express();
const productRouter = require('./routes/products')
const addProduct = require('./routes/addProduct')
const auth = require('./routes/auth')
const login = require('./routes/login')
const editAuth = require('./routes/editAuth')
app.use(cors())
app.use(express.json());

app.use('/', productRouter);
app.use('/', addProduct);
app.use('/', auth);
app.use('/', login);
app.use('/', editAuth);

app.listen(3001,() => console.log("Listen to 3001"))